using DevExpress.ExpressApp.Security;
using Xafari.Security;

namespace Galaktika.HCM.App
{
	partial class HCMAppModule
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
			this.securityStrategyComplex2 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
			this.xafariAuthentication1 = new Xafari.Security.XafariAuthentication();
			// 
			// securityStrategyComplex1
			// 
			this.securityStrategyComplex1.Authentication = null;
			this.securityStrategyComplex1.RoleType = null;
			this.securityStrategyComplex1.UserType = null;
			// 
			// securityStrategyComplex2
			// 
			this.securityStrategyComplex2.Authentication = this.xafariAuthentication1;
			this.securityStrategyComplex2.RoleType = typeof(Xafari.Security.DC.IDCSecuritySystemRole);
			this.securityStrategyComplex2.UserType = typeof(Xafari.Security.DC.IDCSecuritySystemUser);
			// 
			// xafariAuthentication1
			// 
			// 
			// 
			// 
			this.xafariAuthentication1.AuthenticationActiveDirectory.CreateUserAutomatically = true;
			this.xafariAuthentication1.AuthenticationActiveDirectory.LogonParametersType = typeof(Xafari.Security.XafariAuthenticationLogonParameters);
			// 
			// 
			// 
			this.xafariAuthentication1.AuthenticationStandard.LogonParametersType = typeof(Xafari.Security.XafariAuthenticationLogonParameters);
			this.xafariAuthentication1.LogonParametersType = typeof(Xafari.Security.XafariAuthenticationLogonParameters);
			// 
			// HCMAppModule
			// 
			this.ApplicationName = "HCM";
			this.RequiredModuleTypes.Add(typeof(Xafari.XafariModule));
			this.RequiredModuleTypes.Add(typeof(Xafari.Security.DC.XafariSecurityDCModule));
			this.RequiredModuleTypes.Add(typeof(Xafari.BC.CD.XafariBCCDModule));
			this.RequiredModuleTypes.Add(typeof(Galaktika.HCM.ARP.ARPModule));
			this.Security = this.securityStrategyComplex2;

		}

		#endregion

		private SecurityStrategyComplex securityStrategyComplex1;
		private SecurityStrategyComplex securityStrategyComplex2;
		private XafariAuthentication xafariAuthentication1;



	}
}
