using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Xpo;
using Xafari;
using Xafari.AppModules;
using Xafari.Security;

namespace Galaktika.HCM.App
{
	// See http://galaktikasoft.com/documentation/xafari/appmodule.html
	public sealed partial class HCMAppModule : AppModuleBase
	{

		public HCMAppModule()
		{
			InitializeComponent();
		}

		public override void InitializeApplication(XafApplication application)
		{
			base.InitializeApplication(application);

			// add here platform-agnostic code to initialize application instance

			application.LinkNewObjectToParentImmediately = false;
			if (System.Diagnostics.Debugger.IsAttached && application.CheckCompatibilityType == CheckCompatibilityType.DatabaseSchema)
				application.DatabaseUpdateMode = DatabaseUpdateMode.UpdateDatabaseAlways;

			application.CustomizeLanguagesList += this.HCMAppModule_CustomizeLanguagesList;
			application.Xafari().CustomizeApplicationContext += this.HCMAppModule_CustomizeApplicationContext;
			application.DatabaseVersionMismatch += application_DatabaseVersionMismatch;
		}

		void application_DatabaseVersionMismatch(object sender, DatabaseVersionMismatchEventArgs e)
		{
			e.Updater.Update();
			e.Handled = true;
		}

		void HCMAppModule_CustomizeApplicationContext(object sender, Xafari.Helpers.CustomizeApplicationContextEventArgs args)
		{
			// add here additional application context

			//args.Context.Add(XafariApplicationContext.DC);
		}

		// override this method to customize ObjectSpace providers
		protected override void CreateObjectSpaceProviders(CreateCustomObjectSpaceProviderEventArgs args)
		{
			base.CreateObjectSpaceProviders(args);
		}

		private void HCMAppModule_CustomizeLanguagesList(object sender, CustomizeLanguagesListEventArgs e)
		{
			string userLanguageName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			if (userLanguageName != "en-US" && e.Languages.IndexOf(userLanguageName) == -1)
			{
				e.Languages.Add(userLanguageName);
			}
		}

//		private void xafariAuthentication1_AuthenticationActiveDirectory_CreateCustomLogonParameters(object sender, CreateCustomLogonParametersEventArgs e)
//		{
//			xafariAuthentication1.
//		}
	}
}
